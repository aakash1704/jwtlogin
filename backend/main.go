package main

import (
	initializers "jwtLoginApp/initializers"

	"jwtLoginApp/controllers"

	"jwtLoginApp/middleware"

	"github.com/gin-gonic/gin"
	cors "github.com/rs/cors/wrapper/gin"
)

func init() {
	initializers.LoadEnvVariable()
	initializers.ConnectToDb()
	initializers.SyncDatabase()

}

func main() {
	r := gin.Default()

	//r.Use(middleware.CORSMiddleware())
	r.Use(cors.New(cors.Options{
		AllowedOrigins:   []string{"http://localhost:3000"},
		AllowCredentials: true,
	}))
	r.POST("/signup", controllers.Signup)
	r.POST("/login", controllers.Login)
	r.GET("/validate", middleware.AuthMiddleware, controllers.Validate)

	r.Run()

}

package initializers

import (
	"jwtLoginApp/models"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	//"jwtLoginApp/models"
)

// var DB *gorm.DB

// func ConnectToDb() {
// 	var err error
// 	dsn := os.Getenv("DB")

// 	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})

// 	if err != nil {
// 		panic("failed to connect to db")
// 	}

// }

var DB *gorm.DB

func ConnectToDb() {
	var err error
	//DSN := "host=localhost user=postgres dbname=jwtlogin password=Aaka$h1234 sslmode=disable"
	dsn := os.Getenv("DB")
	DB, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("Error:Failed to connect to database!")
	}

	DB.AutoMigrate(&models.User{})

	//DB = db
}

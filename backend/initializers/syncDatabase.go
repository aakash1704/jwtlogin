package initializers

import "jwtLoginApp/models"

func SyncDatabase() {
	DB.AutoMigrate(&models.User{})
}

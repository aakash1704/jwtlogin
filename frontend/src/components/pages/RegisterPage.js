import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";

import "../../App.css";

const SignUpPage = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [username, setUserName] = useState("");
  const navigate = useNavigate()


  const emailHandle = (e) => {
    setEmail(e.target.value);
  };
  const usernameHandle = (e) => {
    setUserName(e.target.value);
  };
  const passwordHandle = (e) => {
    setPassword(e.target.value);
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(email + " " + password);

    await axios
      .post("http://localhost:5000/signup", {
        name: username,
        email,
        password,
      })
      .then((res) => {
        if (res.status === 200) {
            navigate("/login")
        }
      });
  };
  return (
    <div className="text-center m-5-auto">
      <h2>Join us</h2>
      <h5>Create your personal account</h5>
      <form action="/home" onSubmit={(e) => handleSubmit(e)}>
      <p>
          <label>Username</label>
          <br />
          <input
            type="username"
            name="username"
            onChange={usernameHandle}
            required
          />
        </p>
        <p>
          <label>Email address</label>
          <br />
          <input type="email" name="email" onChange={emailHandle} required />
        </p>
        <p>
          <label>Password</label>
          <br />
          <input
            type="password"
            name="password"
            onChange={passwordHandle}
            required
          />
        </p>
       
        <p>
          <input type="checkbox" name="checkbox" id="checkbox" required />{" "}
          <span>
            I agree all statements in{" "} 
            <a
              href="https://google.com"
              target="_blank"
              rel="noopener noreferrer"
            >
              terms of service
            </a>
          </span>
          .
        </p>
        <p>
          <button id="sub_btn" type="submit">
            Register
          </button>
        </p>
      </form>
      <footer>
        <p>
          <Link to="/">Back to Homepage</Link>.
        </p>
      </footer>
    </div>
  );
};

export default SignUpPage;

import React, { useState } from "react";
import { Link,useNavigate } from "react-router-dom";
import axios from "axios";


import "../../App.css";

export default function SignInPage() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate()
  const config = {
    headers: {
      "Content-Type": "application/json"
      },
      withCredentials: true
    }

  const emailHandle = (e) => {
    setEmail(e.target.value);
  };
  const passwordHandle = (e) => {
    setPassword(e.target.value);
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    console.log(email + " " + password);

    await axios
      .post("http://localhost:5000/login", {
        email,
        password,
      },config)
      .then((res) => {
        if(res.status === 200) {
          navigate("/home")
        }
      });
  };
  return (
    <div className="text-center m-5-auto">
      <h2>Sign in to us</h2>
      <form action="/home" onSubmit={(e) => handleSubmit(e)}>
        <p>
          <label>Email address</label>
          <br />
          <input
            onChange={emailHandle}
            type="text"
            name="first_name"
            required
          />
        </p>
        <p>
          <label>Password</label>
          {/* <Link to="/forget-password"><label className="right-label">Forget password?</label></Link> */}
          <br />

          <input
            type="password"
            name="password"
            required
            onChange={passwordHandle}
          />
        </p>
        <p>
          <button id="sub_btn" type="submit">
            Login
          </button>
        </p>
      </form>
      <footer>
        <p>
          First time? <Link to="/register">Create an account</Link>.
        </p>
        <p>
          <Link to="/">Back to Homepage</Link>.
        </p>
      </footer>
    </div>
  );
}
